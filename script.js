function calculateTeamDeadline(tasksPerDeveloper, backlog, deadline) {
    // Обчислюємо загальну кількість робочих годин до дедлайну
    const totalWorkHours = (deadline - new Date()) / (1000 * 60 * 60);
  
    // Обчислюємо загальну кількість робочих годин у команді
    const totalWorkHoursInTeam = tasksPerDeveloper.reduce((acc, tasks) => acc + tasks, 0) * 8;
  
    // Розраховуємо, чи команда зможе завершити всі завдання до дедлайну
    if (totalWorkHours >= totalWorkHoursInTeam) {
      const daysUntilDeadline = Math.floor(totalWorkHoursInTeam / 8 / tasksPerDeveloper.length);
      return `Усі завдання будуть успішно виконані за ${daysUntilDeadline} днів до настання дедлайну!`;
    } else {
      const extraHours = (totalWorkHoursInTeam - totalWorkHours) % (8 * tasksPerDeveloper.length);
      const extraDays = Math.ceil((totalWorkHoursInTeam - totalWorkHours) / (8 * tasksPerDeveloper.length));
      return `Команді розробників доведеться витратити додатково ${extraDays} днів і ${extraHours} годин після дедлайну, щоб виконати всі завдання в беклозі.`;
    }
  }
  
  // Приклад:
  const tasksPerDeveloper = [4, 6, 8];
  const backlog = [12, 24, 20];
  const deadline = new Date('2023-12-31');
  
  const result = calculateTeamDeadline(tasksPerDeveloper, backlog, deadline);
  console.log(result);
  